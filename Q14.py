# -*- coding: utf-8 -*-
#*******************************************************
# Q14.py
# created by Sawada Tatsuki on 2018/02/07.
# Copyright © 2018 Sawada Tatsuki. All rights reserved.
#*******************************************************
# 問題14

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import vpython as vs
import math as mat
import random as rnd

def main():
    setup() #セットアップ
    drow() #繰り返し描画処理

#問題14: 重力に従って動く質点をアニメーションしてみよ．初期位置や初速を変えて，自由落下,打ち上げられたボールなどをアニメーションせよ．地面から打ち上げられてから，再び地面に付くまでの時間T_0を求め，時刻T_0におけるy座標がどれほど正確に求まるか(ほぼ0であってほしい)，調べてみよ．
def drow():
    rate = 100 #時間レート
    ballRad = 2 #質点の半径(見た目だけ)
    #床の設定   
    floorLength = 200
    floorThickness = 0.5
    floorPos = vs.vector(floorLength / 2, - ballRad - floorThickness / 2 , 0)
    floor = vs.box(pos = floorPos,
                   size = vs.vector(floorLength, floorThickness, 5),
                   color = vs.color.black)
    
    x_0 = vs.vector(0, 0, 0) #初期座標
    v_0 = vs.vector(1, 5, 0) #初速
    g = 9.8 #重力加速度
    t = 0 #時間
    
    #質点の設定
    ball = vs.sphere(pos = x_0,
                     radius = ballRad,
                     color = vs.color.white)
    p = ball.pos #質点の参照
        
    while True:
        vs.rate(rate)
        if p.y < 0: #終了条件
            ball.color = vs.color.red
            print("t = %f" % p.y)
            exit() #プログラムを終了
        x = v_0.x * t + x_0.x
        y = - g * t * t / 2 + v_0.y * t + x_0.y
        p.x += x #x座標を進める        
        p.y += y #y座標を進める
        
        ball.pos = p #ボールの座標を更新
        t += 0.01 #時間を進める
        
def setup():
    vs.scene.title = "問題14"
    vs.scene.background = vs.color.white #白背景に
    vs.scene.center = vs.vector(100, 0, 10)
    #vs.scene.autoscale = 0 #カメラの追随を禁止
    
    
if __name__ == "__main__":
    main()
