# -*- coding: utf-8 -*-
#*******************************************************
# Main.py
# created by Sawada Tatsuki on 2018/02/06.
# Copyright © 2018 Sawada Tatsuki. All rights reserved.
#*******************************************************
# 問題12

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
#import vpython as vs
import math as mat
import random as rnd

def main():    
    print("問題12")
    print(montecarlo())

#問題12(おまけ:乱数を用いた積分):
def montecarlo():
    n = 10000 #プロットする点の数
    c = 0 #球内部の点の数
    for i in range(n):
        x = rnd.uniform(-1, 1)
        y = rnd.uniform(-1, 1)
        z = rnd.uniform(-1, 1)
        if x * x + y * y + z * z <= 1:
            c += 1
    return  8 * c / n #体積2*2*2=8の立方体と比較

if __name__ == "__main__":
    main()
