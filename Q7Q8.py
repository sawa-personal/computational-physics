# -*- coding: utf-8 -*-
#*******************************************************
# Main.py
# created by Sawada Tatsuki on 2018/02/06.
# Copyright © 2018 Sawada Tatsuki. All rights reserved.
#*******************************************************
# 問題7,8

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
#import vpython as vs
import math as mat
import random as rnd

def main():
    print("問題7")
    print(integrateExample())

    print("問題8")
    print(integrate(mat.sin, 0, mat.pi))
    print(integrate(mat.sin, 0, 2 * mat.pi))

#問題7(数値積分): 積分 ∫[1,2] √(x^2 ^ 1) dx の(近似)値を求めてみよ．そのために，積分の定義式: ∫[a,b] f(x) dx = lim(⊿→0) Σ[0,n-1]{f(x_i)(x_(i+1)-x_i)}に戻り，後者の近似値を計算する．すなわち， 1.積分区間[a,b]の中に多数の点 a = x_0 < x_1 < … < x_n = bを取る．最も簡単には，n等分すればよい． 2.f(x_i)(x_(i+1)-x_i)の和を計算する．
def integrateExample():
    a = 1
    b = 2
    n = 1000 #刻み幅
    d = (b - a) / n #積分区間を100等分した値

    #被積分関数
    def f(x):
        return mat.sqrt(x * x - 1)

    #面積の総和を求める
    s = 0 #面積の総和
    for i in range(n-1):
        s += f(a + i * d) * d
        
    return s

#問題8(数値積分 II): Pythonでは，(defを使って)定義された関数を他の関数の入力として渡すこともできる．例えば，
'''
def plus_one(x):
    retrun x + 1

def twice(f, x):
    return f(f(x))

print(twice(plut_one, 3))
'''
#のように．ここで，twiceは「任意の関数を」受け取ることができ，それを2回適用することができる．これに習い，前問を拡張して，関数および積分区間a，bを受け取り，積分∫[a,b] f(x) dx を計算する汎用的な関数，integrate
def integrate(f, a, b):
    n = 100 #刻み幅
    d = (b - a) / n #積分区間を100等分した値
    
    #面積の総和を求める
    s = 0 #面積の総和
    for i in range(n-1):
        s += f(a + i * d) * d

    return s

if __name__ == "__main__":
    main()
