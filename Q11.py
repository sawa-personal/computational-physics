# -*- coding: utf-8 -*-
#*******************************************************
# Main.py
# created by Sawada Tatsuki on 2018/02/06.
# Copyright © 2018 Sawada Tatsuki. All rights reserved.
#*******************************************************
# 問題11

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import vpython as vs
import math as mat
import random as rnd

def main():
    setup()
    drow()

#問題11(乱数):乱数を用いて，球面x^2 + y^2 + z^2 = 1の球面上に多数のboxを配置して，以下のようなオブジェを作ってみよう．
def drow():
    n = 600 #箱の数
    for i in range(n):
        d1 = rnd.random() * 2 * mat.pi #角度θ
        d2 = rnd.random() * 2 * mat.pi #角度φ
        x = mat.sin(d1) * mat.cos(d2)
        y = mat.sin(d1) * mat.sin(d2)
        z = mat.cos(d1)
        p = vs.vector(x, y, z) #プロットする座標
        side = 0.05 #箱の一辺の長さ
        vs.box(pos = p, #座標
               size = vs.vector(side, side, side), #大きさ
               axis = p, #軸
               color = vs.color.green) #色

def setup():
    vs.scene.title = "問題11"
    vs.scene.background = vs.color.white #白背景
        
if __name__ == "__main__":
    main()
