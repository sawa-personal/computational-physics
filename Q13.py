# -*- coding: utf-8 -*-
#*******************************************************
# Q13.py
# created by Sawada Tatsuki on 2018/02/07.
# Copyright © 2018 Sawada Tatsuki. All rights reserved.
#*******************************************************
# 問題13

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import vpython as vs
import math as mat
import random as rnd

def main():
    print("問題13")
    setup()    
    drow()

#問題13: VisualPythonの適当な物体を選び，等速直線運動する物体をアニメーションしてみよ．(中略)等速直線運動する物体の他に最低一ケ，「動かない物体」をおいていくと良い．カメラの追随を禁止したければ，scene.autoscale = 0 とする．が，物体がシーン外にはみ出ると，画面から消えてしまうので使う際は注意が必要．
def drow():
    p = vs.vector(0, 0, 0)
    ball = vs.sphere(pos = p,
                     radius = 2,
                     color = vs.color.white)

    solidObj = vs.box(pos = vs.vector(-5, 0, 0),
                      size = vs.vector(4, 4, 4),
                      color = vs.color.black)
    
    while True:
        vs.rate(100)
        ball.pos = p
        p.x += 0.1 #x座標を進める
        p.z -= 0.1 #z座標を進める
        if p.x >= 50: #終了条件
            ball.color = vs.color.red
            break
        
def setup():
    vs.scene.background = vs.color.white #白背景に
    vs.scene.autoscale = 0 #カメラの追随を禁止
    
if __name__ == "__main__":
    main()
