# -*- coding: utf-8 -*-
#*******************************************************
# Main.py
# created by Sawada Tatsuki on 2018/02/06.
# Copyright © 2018 Sawada Tatsuki. All rights reserved.
#*******************************************************
# HelloWorld

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import vpython as vs
import math as mat
import random as rnd

def main():
    vs.box()
    
if __name__ == "__main__":
    main()
