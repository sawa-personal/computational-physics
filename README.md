# computational-science

計算科学の勉強用のプログラムを作成する予定．

## キーワード
計算科学，数理工学

## 環境設定

### VPythonの導入
次のサイトをを参考に導入する．

* [VPython(公式)](http://vpython.org/)
* [【VPython入門】使い方まとめ | アルゴリズム雑記](https://algorithm.joho.info/programming/python/vpython/)

#### Windowsの場合
cmd上にて

> pip install vpython

でインストール可能．

## コンパイル方法
いずれの問題も

> python Main.py

でコンパイル可能．

## 参考サイト
* [物理シミュレーションを通して学ぶプログラミング 田浦健次朗](https://www.eidos.ic.i.u-tokyo.ac.jp/~tau/lecture/computational_physics/)
* [VPython Instructional Videos: 4. Loops and Animation](https://www.youtube.com/watch?v=cNdPqgNFeVk) vpythonでのアニメーションの作り方
* [VPython Instructional Videos: 5. Scalefactors](https://www.youtube.com/watch?v=wKGAfQrlYaU) ばねマスアニメーション作成の参考
* [Projectile Motion With Visual Python](https://www.youtube.com/watch?v=fzS8jaeI6BM) 放物運動の参考
