# -*- coding: utf-8 -*-
#*******************************************************
# Main.py
# created by Sawada Tatsuki on 2018/02/06.
# Copyright © 2018 Sawada Tatsuki. All rights reserved.
#*******************************************************
# 問題9,10

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
#import vpython as vs
import math as mat
import random as rnd

def main():
    print("問題9")
    print(ODEExample())

    print("問題10")
    print(ODE())
    
#問題9(微分方程式):
def ODEExample():
    y_0 = 1000
    
    return 1

#問題10(微分方程式):
def ODE():
    return 1

if __name__ == "__main__":
    main()
