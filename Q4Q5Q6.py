# -*- coding: utf-8 -*-
#*******************************************************
# Main.py
# created by Sawada Tatsuki on 2018/02/06.
# Copyright © 2018 Sawada Tatsuki. All rights reserved.
#*******************************************************
# 問題4,5,6

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
#import vpython as vs
import math as mat
import random as rnd

def main():
    print("問題4")
    print(a(0))
    print(a(3))
    print(a(6))

    print("問題5")
    print(cbrt(1))
    print(cbrt(8))
    print(cbrt(27))

    print("問題6")
    print(a2(0))
    print(a2(1))
    print(a2(2))
    print(a2(3))
    print(a2(4))

    
#問題1: 4つの数a,b,c,dを受け取り，ベクトル(a,b)と(c,d)の内積を計算する関数inner(a, b, c, d)を書き，適当な例を使って確かめよ．
def inner(a, b, c, d):
    v1 = np.array([a, b])
    v2 = np.array([c, d])
    return np.dot(v1, v2)

#問題4: 変数nを受け取り，次の漸化式で定まる数列のa_nを計算する関数a(n)を書き，適当な例を作って確かめよ．a_0 = 1，a_n = 2 a_(n-1) + 1 (n > 0)．
def a(n):
    if n == 0:
        return 1
    else:
        return 2 * a(n - 1) + 1

#問題5(ニュートン法): 次は，cの3乗根(3√c)に収束する数列である．c_0 = 1, c_n = 2 a_(n-1) + 1 (n > 0)．ある程度大きなnに対して，a_nを計算すればそれが，3√cの近似値となる．これを用いて，与えられた数cに対し，3√cの近似値を計算する関数，cbrt(c)(cubic root)を書け．適当な数の3乗根を計算し，再び3乗して，結果が正しそうか確かめよ．
def cbrt(c):
    return 1

#問題5・参考:　一般に，f(x)=0の解を求めるのに以下のニュートン法が用いられる．a_0 = c，a_n = a_(n-1) - f(a_(n-1))/f'(a_(n-1)) (n > 0)．これが"収束するならば"，x = x - f(x) / f'(x)を満たすx，すなわちf(x) = 0を満たすxに収束することは明らか．本問はこれを，f(x) = x^3 - cに適用しただけ．ただし，収束するとは限らないので無条件に使えるわけではない．計算機で何かを求めるときに，「答えに収束するような数列」を使って計算することは非常に多い．
def newton(x):
    return 1

#問題6: 少し複雑な変数の使い方に慣れる問題．変数nを受け取り，次の漸化式で定まる数列のa_nを計算するa(n)を書き，適当な例を使って確かめよ．
def a2(n):
    if n == 0:
        return 1
    if n == 1:
        return 1
    
    a0 = 1 #メモ
    a1 = 1 #メモ
    #動的計画法
    tmp = 0
    for i in range(2, n+1):
        tmp = a1 + 2 * a0
        a0 = a1
        a1 = tmp
    return tmp #配列の末尾を返す

if __name__ == "__main__":
    main()
