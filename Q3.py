# -*- coding: utf-8 -*-
#*******************************************************
# Main.py
# created by Sawada Tatsuki on 2018/02/06.
# Copyright © 2018 Sawada Tatsuki. All rights reserved.
#*******************************************************
# 問題3

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import vpython as vs
import math as mat
import random as rnd

def main():
    setup()    
    drow()

#問題3: 正四面体の中に一個づつ球面がついている．以下のようなオブジェをvisual pythonで作れ(辺はcurveを適当につなげて書く)．現在やっていることと関係無いように見えるが，「変数」を使うとすっきりと書ける例としてとりあげる．
def drow():
    rad = 0.1 #球の半径
    
    #頂点座標
    p1 = vs.vector(0, 0, mat.sqrt(3) / 2)
    p2 = vs.vector(-0.5, 0, 0)
    p3 = vs.vector(0.5, 0, 0)
    p4 = vs.vector(0, mat.sqrt(3) / 2, mat.sqrt(3) / 2 -  1 / mat.sqrt(3))
    
    #頂点
    vs.sphere(pos = p1,
                    radius = rad,
                    color = vs.color.white)
    vs.sphere(pos = p2,
                    radius = rad,
                    color = vs.color.red)
    vs.sphere(pos = p3,
                    radius = rad,
                    color = vs.color.yellow)
    vs.sphere(pos = p4,
                    radius = rad,
                    color = vs.color.blue)

    #辺
    vs.curve(p1, p2)
    vs.curve(p1, p3)
    vs.curve(p1, p4)
    vs.curve(p2, p3)
    vs.curve(p2, p4)
    vs.curve(p3, p4)
    
def setup():
    vs.scene.title = "問題3"
    vs.scene.background = vs.color.white #白背景に
    
if __name__ == "__main__":
    main()
