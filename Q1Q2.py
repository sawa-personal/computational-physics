# -*- coding: utf-8 -*-
#*******************************************************
# Main.py
# created by Sawada Tatsuki on 2018/02/06.
# Copyright © 2018 Sawada Tatsuki. All rights reserved.
#*******************************************************
# 問題1，2

import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
#import vpython as vs
import math as mat
import random as rnd

def main():
    print("問題1")
    print(inner(1, 0, 1, 1))
    
    print("問題2")
    print(angle(1, 0, 1, 1))
    
#問題1: 4つの数a,b,c,dを受け取り，ベクトル(a,b)と(c,d)の内積を計算する関数inner(a, b, c, d)を書き，適当な例を使って確かめよ．
def inner(a, b, c, d):
    v1 = np.array([a, b])
    v2 = np.array([c, d])
    return np.dot(v1, v2)

#問題2: 4つの数a,b,c,dを受け取り，ベクトル(a,b)と(c,d)のなす角を計算する関数angle(a, b, c, d)を書き，適当な例を使って確かめよ．前門で用いたinnerを使う．また，arccosはmathモジュールに，acosという関数が提供されている．
def angle(a, b, c, d):
    innerVal = inner(a, b, c, d)    
    v1 = np.array([a, b])
    v2 = np.array([c, d])
    v1norm = np.linalg.norm(v1)
    v2norm = np.linalg.norm(v2)
    product = v1norm * v2norm
    cos = innerVal / product
    return mat.acos(cos) / mat.pi * 180

if __name__ == "__main__":
    main()
